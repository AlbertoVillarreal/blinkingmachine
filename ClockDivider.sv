/********************************************************************
* Name:
*	ClockDivider.sv
*
* Description:
* 	This module is a structural clock divider
*
* Inputs:
*	clk: 							board clock.
*	reset:						asynchronous reset.
* Outputs:
* 	clock_signal				1Hz clock
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	10/2/2018 
*
*********************************************************************/

module ClockDivider
#
(
	parameter boardFrecuency = 50000000,
	parameter iFrecuency = 25000000,
	parameter count = countDivider(iFrecuency)
)
(
	//Inputs
	input clk,
	input reset,
	//Outputs
	output clock_signal
);

bit oneShot_b;//wire for counterWithFunction module
bit clkSignal_b;//wire for toogle module output

CounterWithFunction 
#(
	// Parameter Declarations
	.MAXIMUM_VALUE(count),
	.NBITS_FOR_COUNTER(CeilLog2(count))
)
oneShot
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	
	// Output Ports
	.flag(oneShot_b)
);

Toggle clock_toggle
(
	//Inputs
	.oneShot(oneShot_b),
	.clk(clk),
	.reset(reset),
	
	//outputs
	.clkSignal(clkSignal_b)
);

assign clock_signal = clkSignal_b;

/***************************Compiler directiver***********************************/

   //Function to get number of clocks to get wished frecuency
function integer countDivider;

	input integer hertz;
	countDivider = boardFrecuency / ( 2 * hertz );
	
endfunction

 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i <= data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

endmodule

