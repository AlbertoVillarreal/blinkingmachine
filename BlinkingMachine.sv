/********************************************************************
* Name:
*	BlinkingMachine.sv
*
* Description:
* 	This module is a blinking machine that use the clock divier to modify the clock and 
*  have a 1Hz signal. This signal enter to the machine and the output of the machine is
*  a trigger that makes a led blink each second depending of the output.
*
* Inputs:
*	clk: 							board clock.
*	reset:						asynchronous reset.
*  Start:						signal that tell the machine to start.
* Outputs:
* 	Out:							Output of the machine.
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	10/2/2018 
*
*********************************************************************/
module BlinkingMachine
(
	//Inputs	
	input clk,
	input reset,
	input start,
	
	//Outputs
	output out
	
	);
	
	bit out_bit;//Output of the moore machine.
	bit clock_divider_output_bit;//Output of the clock divider.
	
	ClockDivider This_is_clock_divider
	(
		//inputs
		.clk(clk),
		.reset(reset),
		//outputs
		.clock_signal(clock_divider_output_bit)
	);
	
	QP_MooreStateMachine This_is_moore_machine
	(
		
		// Input Ports
		.clk(clock_divider_output_bit),
		.reset(reset),
		.Start(start),

		// Output Ports
		.out(out_bit)
	);
	
	assign out = out_bit;
	assign clk_1hz = clock_divider_output_bit;
	

endmodule