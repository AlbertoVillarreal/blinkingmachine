 
module QP_MooreStateMachine
#(	
	parameter countON = 6,
	parameter countOFF = 4
)
(
	// Input Ports
	input clk,
	input reset,
	input Start,

	// Output Ports
	output bit out
);


enum logic [2:0] {IDLE, ON, OFF} state, next_state; 
logic [2:0] count;
bit signal_b;

/*------------------------------------------------------------------------------------------*/

/*Asignacion de estado, proceso secuencial*/
always_ff@(posedge clk, negedge reset) begin: Assigning_state

	if(reset == 1'b0)
	begin
			state <= IDLE;
			signal_b <= 1'b0;
			count <= 3'b0;
	end
	else 
	begin
			count <= count + 1;
			state <= next_state;
			if(state == ON)
			begin
				if(count == countON - 1)
				begin
					signal_b <= 1;
					count <= 0;
				end
				else
					signal_b <= 0;
			end
			
			else
			begin
				if(state == OFF)
				begin
					if(count == countOFF - 1)
					begin
						signal_b <= 0;
						count <= 0;
					end
					else
						signal_b <= 1;
				end
			end

	end
	
	

end: Assigning_state



/*Asignacion de estado, proceso secuencial*/
always_comb begin	
	 
	 next_state = IDLE;
	 
	 case(state)
		
			IDLE:
				if(Start == 1'b1)
					next_state = ON;
				else
					next_state = IDLE;					
			ON:
				if (signal_b == 1'b0)
					next_state = ON;
				else 
					next_state = OFF;	
			OFF:		
				if (signal_b == 1'b1)
					next_state = OFF;
				else 
					next_state = ON;	
					
					
			default:
					next_state = IDLE;

			endcase
end//end always
/*------------------------------------------------------------------------------------------*/
/*Asignación de salidas,proceso combinatorio*/
always_comb begin
	
	case(state)
		IDLE: 
				out = 1'b0;
		ON: 
				out = 1'b1;
		OFF:
				out = 1'b0;
		
	default: 		
				out = 1'b0;

	endcase
end

endmodule
